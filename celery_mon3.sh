#!/bin/bash

#############Settings section#############
#username of processes parent
user=deploy

#celery project, sample: "celery worker -A somename" - somename is project
proj=celery_app

#path to python.bin for celery
workdir=.env/bin/python

#path to celery bin file from ps wauxf 
celerybin=celery

#select mode of work. 1 - from user, 2 - from root + supervisord;
mode=1
dirmode1=/var/apps/api
############Action section###############
mkdir /tmp/essm_celery

case $mode in
	1)
		all_nodes=$(su - $user -s /bin/bash -c "cd $dirmode1; $workdir $celerybin -A $proj status")
;;
	2)
all_nodes=$(env PYTHONPATH="$PYTHONPATH:$workdir" $celerybin -A $proj status)
	esac

echo $all_nodes > /tmp/essm_celery/all_nodes.tmp
sed 's/OK/OK\r\n/g' /tmp/essm_celery/all_nodes.tmp > /tmp/essm_celery/.all_nodes.tmp2 && mv /tmp/essm_celery/.all_nodes.tmp2 /tmp/essm_celery/all_nodes.tmp
cat /tmp/essm_celery/all_nodes.tmp | awk -F ":" '{print $1}' | while read line
			do
case $mode in
	1) #mode1
	        su - $user -s /bin/bash -c "cd $dirmode1; $workdir $celerybin -A $proj inspect scheduled -d $line" >> /tmp/essm_celery/sheduled
	        su - $user -s /bin/bash -c "cd $dirmode1; $workdir $celerybin -A $proj inspect revoked -d $line" >> /tmp/essm_celery/failed
	        su - $user -s /bin/bash -c "cd $dirmode1; $workdir $celerybin -A $proj inspect active -d $line" >> /tmp/essm_celery/active
		;;
	2) #mode2
	env PYTHONPATH="$PYTHONPATH:$workdir" $celerybin -A $proj inspect scheduled -d $line >> /tmp/essm_celery/sheduled
		env PYTHONPATH="$PYTHONPATH:$workdir" $celerybin -A $proj inspect revoked -d $line >> /tmp/essm_celery/failed
		env PYTHONPATH="$PYTHONPATH:$workdir" $celerybin -A $proj inspect active -d $line  >> /tmp/essm_celery/active
		;;
	esac		
	done

	all_nodes_count=$(cat /tmp/essm_celery/all_nodes.tmp | grep -E "node|nodes online" | awk '{print $1}')
			sheduled_summ=$(cat /tmp/essm_celery/sheduled | grep -c \*)
			echo sheduled:$sheduled_summ >> /tmp/essm_celery/final2
			failed_summ=$(cat /tmp/essm_celery/failed | grep -c \*)
			echo failed:$failed_summ >> /tmp/essm_celery/final2
			active_summ=$(cat /tmp/essm_celery/active| grep -c \*)
			echo active:$active_summ >> /tmp/essm_celery/final2
			echo online:$all_nodes_count >>  /tmp/essm_celery/final2

			rm -rf /tmp/essm_celery/active
			rm -rf /tmp/essm_celery/failed
			rm -rf /tmp/essm_celery/sheduled
			mv /tmp/essm_celery/final2 /tmp/essm_celery/final



####info section#####
#cat /tmp/essm_celery/final
#* * * * * /bin/bash /root/scripts/celery_mon.sh
###customs section####
#custom celery_metrics sheduled_task cat /tmp/essm_celery/final | grep sheduled | awk -F ":" '{print $2}'
#custom celery_metrics failed_task cat /tmp/essm_celery/final | grep failed | awk -F ":" '{print $2}'
#custom celery_metrics active_task cat /tmp/essm_celery/final | grep active | awk -F ":" '{print $2}'
#custom celery_metrics online_nodes cat /tmp/essm_celery/final | grep online | awk -F ":" '{print $2}'
#custom celery_metrics list_upgrade_is_ok alert=<0_5m find /tmp/essm_celery/ -iname final -type f -mmin 1 | wc -l

####description's
#failed: alert>1; Кол-во отклоненных задач в нодах. Проверяется в файле по пути /tmp/essm_celery/failed
#online: alert<n-1; Кол-во рабочих нод в celery
#active: alert<0; С нодами, похоже, что-то не так. Пропали активные задачи. Проверяется в файле по пути /tmp/essm_celery/active
#sheduled: alert>300; Задачи не выполняются нодами. Проверяется в файле по пути /tmp/essm_celery/active
